<?php


namespace AppBundle\Service;


class ImageUploader
{
    private $imageDirectory;

    public function __construct($imageDirectory)
    {
        $this->imageDirectory = $imageDirectory;
    }

    public function upload($file) {
        $fileName =  md5(uniqid()) . '.' . $file->guessExtension();
        $file->move(
            $this->imageDirectory,
            $fileName
        );
        return $fileName;
    }
}