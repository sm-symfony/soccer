<?php


namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\Player;
use AppBundle\Entity\Team;
use AppBundle\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadFixtures extends Fixture implements FixtureInterface, ContainerAwareInterface
{
    private $container;

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();


        // Get our userManager, you must implement `ContainerAwareInterface`
        $userManager = $this->container->get('fos_user.user_manager');

        // Create our user and set details
        $user = $userManager->createUser();
        $user->setUsername('admin');
        $user->setEmail('admin@admin.com');
        $user->setPlainPassword('admin');
        //$user->setPassword('3NCRYPT3D-V3R51ON');
        $user->setEnabled(true);
        $user->setRoles(array('ROLE_USER'));

        // Update the user
        $userManager->updateUser($user, true);

        $teams = [
            'Real Madrid C. F.' => 'real-madrid.png',
            'Manchester United F. C.' => 'manchester-united.png',
            'FC Bayern Munich' => 'bayern.png',
            'Liverpool F. C.' => 'liverpool.jpg',
            'Chelsea F. C.' => 'chelsea.png',
        ];

        foreach ($teams as $key => $value) {
            $team = new Team();
            $team->setName($key);
            $team->setLogoUri($value);
            $manager->persist($team);
        }

        $manager->flush();

        $teams = $manager->getRepository('AppBundle:Team')
            ->findAll();

        for ($i = 0; $i < 20; $i++) {
            $player = new Player();
            $player->setFirstName($faker->firstName);
            $player->setLastName($faker->lastName);
            $player->setImageUri('bayern.png');
            $player->setTeam($faker->randomElement($teams));
            $manager->persist($player);
        }

        $manager->flush();
    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
}