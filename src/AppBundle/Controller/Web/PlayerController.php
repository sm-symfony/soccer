<?php

namespace AppBundle\Controller\Web;

use AppBundle\Entity\Player;
use AppBundle\Form\PlayerFormType;
use AppBundle\Service\ImageUploader;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class PlayerController
 * @Security("has_role('ROLE_USER')")
 */
class PlayerController extends Controller
{
    /**
     * @Route("/player", name="player_list")
     */
    public function listAction()
    {
        $players = $this->getDoctrine()
            ->getRepository('AppBundle:Player')
            ->findAll();
        return $this->render('player/list.html.twig', [
            'players' => $players
        ]);
    }

    /**
     * @Route("/player/new", name="player_new")
     */
    public function newAction(Request $request)
    {
        $player = new Player();
        $form = $this->createForm(PlayerFormType::class, $player);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $file = $player->getImageUri();
            $imageUploader = new ImageUploader($this->getParameter('image_directory'));
            $fileName = $imageUploader->upload($file);
            $player->setImageUri($fileName);
            $player = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($player);
            $em->flush();
            $this->addFlash('success', 'Player Created!!');
            return $this->redirectToRoute('player_list');
        }

        return $this->render('player/new.html.twig', [
            'playerForm' => $form->createView()
        ]);
    }

    /**
     * @Route("player/{id}/show", name="player_show")
     */
    public function showAction(Player $player)
    {
        return $this->render('player/show.html.twig', [
            'player' => $player
        ]);
    }

    /**
     * @Route("player/{id}/edit", name="player_edit")
     */
    public function editAction(Request $request, Player $player)
    {
        $form = $this->createForm('AppBundle\Form\PlayerFormType', $player);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $player->getImageUri();
            $imageUploader = new ImageUploader($this->getParameter('image_directory'));
            $fileName = $imageUploader->upload($file);
            $player->setImageUri($fileName);
            $player = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($player);
            $em->flush();
            $this->addFlash('success', 'Player Updated!!');
            return $this->redirectToRoute('player_list');
        }

        return $this->render('player/edit.html.twig', [
            'playerForm' => $form->createView(),
            'player' => $player
        ]);
    }

    /**
     * @Route("player/{id}/delete", name="player_delete", methods={"GET"})
     */
    public function deleteAction(Player $player)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($player);
        $em->flush();

        $this->addFlash('success', 'Player Deleted!!');

        return $this->redirectToRoute('player_list');
    }
}