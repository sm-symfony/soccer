<?php

namespace AppBundle\Controller\Web;

use AppBundle\Entity\Team;
use AppBundle\Form\TeamFormType;
use AppBundle\Service\ImageUploader;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class TeamController
 * @Security("has_role('ROLE_USER')")
 */
class TeamController extends Controller
{
    /**
     * @Route("/team", name="team_list")
     */
    public function listAction()
    {
        $teams = $this->getDoctrine()
            ->getRepository('AppBundle:Team')
            ->findAll();
        return $this->render('team/list.html.twig', [
            'teams' => $teams
        ]);
    }

    /**
     * @Route("/team/new", name="team_new")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $team = new Team();
        $form = $this->createForm(TeamFormType::class, $team);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $file = $team->getLogoUri();
            $imageUploader = new ImageUploader($this->getParameter('image_directory'));
            $fileName = $imageUploader->upload($file);
            $team->setLogoUri($fileName);
            $team = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($team);
            $em->flush();
            $this->addFlash('success', 'Team Created!!');
            return $this->redirectToRoute('team_list');
        }

        return $this->render('team/new.html.twig', [
            'teamForm' => $form->createView()
        ]);
    }

    /**
     * @Route("/team/{id}/show", name="team_show")
     */
    public function showAction(Team $team)
    {
        return $this->render('team/show.html.twig', [
            'team' => $team
        ]);
    }

    /**
     * @Route("/team/{id}/edit", name="team_edit")
     */
    public function editAction(Request $request, Team $team)
    {
        $form = $this->createForm('AppBundle\Form\TeamFormType', $team);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $team->getLogoUri();
            $imageUploader = new ImageUploader($this->getParameter('image_directory'));
            $fileName = $imageUploader->upload($file);
            $team->setLogoUri($fileName);
            $team = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($team);
            $em->flush();
            $this->addFlash('success', 'Team Updated!!');
            return $this->redirectToRoute('team_list');
        }

        return $this->render('team/edit.html.twig', [
            'teamForm' => $form->createView(),
            'team' => $team
        ]);
    }

    /**
     * @Route("team/{id}/delete", name="team_delete", methods={"GET"})
     */
    public function deleteAction(Request $request, Team $team)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($team);
        $em->flush();

        $this->addFlash('success', 'Team Deleted!!');

        return $this->redirectToRoute('team_list');
    }
}