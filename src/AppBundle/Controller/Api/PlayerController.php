<?php


namespace AppBundle\Controller\Api;


use AppBundle\Entity\Player;
use AppBundle\Entity\Team;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class PlayerController extends Controller
{
    /**
     * @Route("/api/players/{id}", methods={"GET"})
     * @param integer $id
     * @return JsonResponse
     */
    public function listAction(Team $team) {
        $players = $team->getPlayers();
        $data = [
            'team' => [
                'name' => $team->getName(),
                'logoUri' => $team->getLogoUri(),
                'players' => []
            ]
        ];

        foreach ($players as $player) {
            $data['team']['players'][] = $this->serializeTeams($player);
        }

        return new JsonResponse($data);
    }

    private function serializeTeams(Player $player) {
        return [
            'firstName' => $player->getFirstName(),
            'lastName' => $player->getLastName(),
            'profilePicture' =>  $player->getImageUri()
        ];
    }
}