<?php


namespace AppBundle\Controller\Api;

use AppBundle\Entity\Team;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class TeamController extends Controller
{
    /**
     * @Route("/api/teams", methods={"GET"})
     */
    public function listAction() {
        $teams = $this->getDoctrine()
            ->getRepository('AppBundle:Team')
            ->findAll();
        $data = ['teams' => []];
        foreach ($teams as $team) {
            $data['teams'][] = $this->serializeTeams($team);
        }
        return new JsonResponse($data);
    }

    private function serializeTeams(Team $team) {
        return [
            'id' => $team->getId(),
            'name' => $team->getName(),
            'logoUri' => $team->getLogoUri()
        ];
    }
}