<?php

namespace AppBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PlayerFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName')
            ->add('lastName')
            ->add('imageUri', FileType::class, [
                'label' => 'Profile Picture',
                'data_class' => null
            ])
            ->add('team', EntityType::class, [
                'class' => 'AppBundle\Entity\Team',
                'placeholder' => 'Choose a team'
            ])
            ->add('save', SubmitType::class, [
                'attr' => ['class' => 'btn btn-success btn-lg pull-left', 'formnovalidate' => 'true']
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\Player'
        ]);
    }

    public function getBlockPrefix()
    {
        return 'app_bundle_player_form_type';
    }
}
