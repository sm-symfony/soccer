<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class TeamFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('logoUri', FileType::class, [
                'label' => 'Logo',
                'data_class' => null
            ])
            ->add('save', SubmitType::class, [
                'attr' => ['class' => 'btn btn-success btn-lg pull-left', 'formnovalidate' => 'true']
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\Team'
        ]);
    }

    public function getBlockPrefix()
    {
        return 'app_bundle_team_form_type';
    }
}
