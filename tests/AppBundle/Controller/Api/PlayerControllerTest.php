<?php

namespace Tests\AppBundle\Controller\Api;

use Tests\AppBundle\Test\ApiTestCase;

class PlayerControllerTest extends ApiTestCase
{
    public function testList()
    {
        $team1 = $this->createTeam([
            'name' => 'Real Madrid C. F.',
            'logoUri' => 'real-madrid.png'
        ]);

        $this->createTeam([
            'name' => 'Manchester United F. C.',
            'logoUri' => 'manchester-united.png'
        ]);

        $this->createPlayer([
            'firstName' => 'Rodger',
            'lastName' => 'Haag',
            'imageUri' => 'bayern.png',
            'team' => $team1
        ]);

        $this->createPlayer([
            'firstName' => 'Alessandro',
            'lastName' => 'Murazik',
            'imageUri' => 'bayern.png',
            'team' => $team1
        ]);

        $response = $this->client->get($this->client->getConfig('base_url') . 'api/players/' . $team1->getId());
        $this->assertEquals('200', $response->getStatusCode());
        $this->assertEquals('application/json', $response->getHeaderLine('content-type'));
        $finishData = json_decode($response->getBody(), true);
        $this->assertArrayHasKey('team',$finishData);
        $this->assertEquals('Real Madrid C. F.', $finishData['team']['name']);
        $this->assertEquals('real-madrid.png', $finishData['team']['logoUri']);
        $players = array_slice($finishData['team']['players'], 0, 2);
        $arr = [
            ['firstName' => 'Rodger', 'lastName' => 'Haag', 'profilePicture' => 'bayern.png'],
            ['firstName' => 'Alessandro', 'lastName' => 'Murazik', 'profilePicture' => 'bayern.png']
        ];
        $this->assertArraySubset($arr, $players);
    }
}