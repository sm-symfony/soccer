<?php

namespace Tests\AppBundle\Controller\Api;

use Tests\AppBundle\Test\ApiTestCase;

class TeamControllerTest extends ApiTestCase
{
    public function testList()
    {
        $this->createTeam([
            'name' => 'Real Madrid C. F.',
            'logoUri' => 'real-madrid.png'
        ]);

        $this->createTeam([
            'name' => 'Manchester United F. C.',
            'logoUri' => 'manchester-united.png'
        ]);

        $response = $this->client->get($this->client->getConfig('base_url') . 'api/teams');
        $this->assertEquals('200', $response->getStatusCode());
        $this->assertEquals('application/json', $response->getHeaderLine('content-type'));
        $finishData = json_decode($response->getBody(), true);
        $this->assertArrayHasKey('teams',$finishData);
        $team = array_slice($finishData['teams'], 0, 2);
        $arr = [
            ['name' => 'Real Madrid C. F.', 'logoUri' => 'real-madrid.png'],
            ['name' => 'Manchester United F. C.', 'logoUri' => 'manchester-united.png']
        ];
        $this->assertArraySubset($arr, $team);
    }
}