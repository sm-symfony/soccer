<?php

namespace Tests\AppBundle\Controller\Web;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Soccer', $crawler->filter('a.navbar-brand')->text());
        $this->assertContains('Home', $crawler->filter('a.nav-link')->text());
        $this->assertContains('Login', $crawler->filter('a.btn.btn-outline-light')->text());
        $this->assertContains('If you are authorized then just login and create your favorite Teams & Players', $crawler->filterXPath('html/body/main/div/p[1]')->text());
        $this->assertContains('If NOT just check Teams & Players', $crawler->filterXPath('html/body/main/div/p[2]')->text());
        $this->assertContains('Click for Teams', $crawler->filter('button#js-load-team')->text());
        $this->assertContains('Welcome To Soccer World!', $crawler->filter('h1.display-1')->text());
    }
}