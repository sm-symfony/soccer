<?php


namespace Tests\AppBundle\Test;


use AppBundle\Entity\Player;
use AppBundle\Entity\Team;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\ORM\EntityManager;
use GuzzleHttp\Client;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\PropertyAccess\PropertyAccess;

class ApiTestCase extends KernelTestCase
{
    private static $staticClient;

    /**
     * @var Client
     */
    protected $client;

    public static function setUpBeforeClass()
    {
        self::$staticClient = new Client([
            'base_url' => 'http://localhost:8000/app_test.php/',
            'defaults' => [
                'exceptions' => false
            ]
        ]);

        self::bootKernel();
    }

    public function setUp()
    {
        $this->client = self::$staticClient;
        $this->purgeDatabase();
    }

    protected function tearDown()
    {

    }

    private function purgeDatabase()
    {
        $purger = new ORMPurger($this->getService('doctrine')->getManager());
        $purger->purge();
    }

    protected function getService($id)
    {
        return self::$kernel->getContainer()->get($id);
    }

    /**
     * @return EntityManager
     */
    protected function getManager()
    {
        return $this->getService('doctrine.orm.default_entity_manager');
    }

    protected function createTeam(Array $data)
    {
        $accessor = PropertyAccess::createPropertyAccessor();
        $team = new Team();
        foreach ($data as $key => $value) {
            $accessor->setValue($team, $key, $value);
        }
        $this->getManager()->persist($team);
        $this->getManager()->flush();
        return $team;
    }


    protected function createPlayer(Array $data)
    {
        $accessor = PropertyAccess::createPropertyAccessor();
        $player = new Player();
        foreach ($data as $key => $value) {
            $accessor->setValue($player, $key, $value);
        }
        $this->getManager()->persist($player);
        $this->getManager()->flush();
        return $player;
    }
}