Soccer
======

#Installation

1. Update parameters.yml for database credentials
2. ``composer install``
3. ``bin/console doctrine:fixtures:load``

#Notes
1. "Clicks for teams" button will show functionality mentioned in Stroy 3 of assignment,
2. Use username: admin & password admin for login or create using ``php bin/console fos:user:create``.
3. There is some issue with http://lorempixel.com/ that's why player profile image is not showing through fixtures.
4. Team API : GET /api/teams
5. Player API: GET /api/players/{team_id}

#Running API tests
1. ``./bin/console doctrine:database:create --env=test``
2. ``./bin/console doctrine:migrations:migrate --env=test``
3. ``./vendor/bin/simple-phpunit tests/AppBundle/Controller/Api/``