const loadTeamButton = document.getElementById('js-load-team');

if (loadTeamButton) {
    loadTeamButton.addEventListener('click', e => {
        fetch('/api/teams')
        .then(function (res) {
            return res.json();
        })
        .then(data => {
            let output = `<table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Logo</th>
                                </tr>
                                </thead>
                                <tbody>`;
            data.teams.forEach(team => {
                output += `<tr>
                                <th scope="row">${team.id}</th>
                                <td><a href="" class="load-players" id="${team.id}">${team.name}</a></td>
                                <td><img src="/uploads/images/${team.logoUri}" alt="${team.name}" height="50px" width="50px"></td>
                            </tr>`;
            });
            output += `</tbody></table>`;
            document.querySelector('.modal-content').innerHTML = output;
        })
        .catch(err => {
            console.log(err);
        });
    });
}

const loadPlayers = document.querySelector('.modal-content');

if (loadPlayers) {
    loadPlayers.addEventListener('click', e => {
        if (e.target.classList.contains('load-players')) {
            fetch(`/api/players/${e.target.id}`)
                .then(function (res) {
                    return res.json();
                })
                .then(data => {
                    let output = `<div class="mx-auto" style="width: 18rem;">
                                    <img class="card-img-" src="/uploads/images/${data.team.logoUri}" alt="Card image cap" height="100px" width="100px">
                                        <h1>${data.team.name}</h1>
                                  </div>
                                  <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th>Profile Picture</th>
                                        </tr>
                                        </thead>
                                        <tbody>`;
                    data.team.players.forEach(player => {
                        output += `<tr>
                                <td>${player.firstName}</td>
                                <td>${player.lastName}</td>
                                <td><img src="/uploads/images/${player.profilePicture}" alt="${player.firstName}" height="50px" width="50px"></td>
                            </tr>`;
                    });
                    output += `</tbody></table>`;
                    document.querySelector('.modal-content').innerHTML = output;
                })
                .catch(err => {
                    console.log(err);
                });
        }
        e.preventDefault();
    });
}